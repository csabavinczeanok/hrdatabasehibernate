package or.example.hrdatabasehibernate;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import or.example.hrdatabasehibernate.model.Employee;
import or.example.hrdatabasehibernate.model.Job;

public class App {

	private static SessionFactory sessionFactory;

	public static void main(String[] args) {
		sessionFactory = new Configuration().configure().buildSessionFactory();
		
//		getAllEmployeess();
//		getEmployeeByFirstName("Neena");
//		System.out.println(getEmployeeById(50));
//		getAllJobs();
		
		Employee emp = new Employee(25, "Ramona", "Cristea", "ramo@ceva", "0573", 
			LocalDate.of(2019, 01, 01), "IT_ENG", 15000);
		saveEmployee(emp);
		System.out.println(getEmployeeById(25));
		
		sessionFactory.close();
	}
	
	public static void getAllEmployeess() {
		Session session = sessionFactory.openSession();
		Transaction tx  = session.beginTransaction();
		
		Query query = session.createQuery("from Employee");
		List<Employee> employees = query.getResultList();
		
		for(Employee employee : employees) {
			System.out.println(employee);
		}
		tx.commit();
		session.close();
	}
	
	public static void getEmployeeByFirstName(String firstName) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Query query = session.createQuery("from Employee e where e.firstName =: firstName");
		query.setParameter("firstName", firstName);
		
		List<Employee> employees = query.getResultList();
		
		System.out.println("------------Employees by first name -----------------");
		
		for(Employee employee : employees) {
			System.out.println(employee);
		}
		
		tx.commit();
		session.close();
	}
	
	public static Employee getEmployeeById(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Employee employee = session.find(Employee.class, id);
		
		tx.commit();
		session.close();
		
		return employee;
		
	}
	
	public static void getAllJobs() {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Query query = session.createQuery("from Job");
		List<Job> jobs = query.getResultList();
		
		for(Job job : jobs) {
			System.out.println(job);
	}
	}
	
	public static void saveEmployee(Employee employee) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		session.save(employee);
		
		tx.commit();
		session.close();
		
	}
	
}